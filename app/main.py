from fastapi import FastAPI

from app.routers import users

app = FastAPI()

app.include_router(users.router, prefix="/users", tags=["users"])

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="localhost", port=8000)